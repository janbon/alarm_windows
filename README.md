***here goes the details***


What is all about
=================

This Python script allow to trigger some alarm on windows.
The alarm uses the .mp3 musics.


To use
------

With windows task scheduler, create a task that will launch the alarm.py script.
You should check the box about launching the task even when not logged. As well as allowing app to turn the machine ON.




Source
------



https://www.howtogeek.com/204742/how-to-make-any-computer-boot-up-or-shut-down-on-a-schedule/


https://answers.microsoft.com/en-us/windows/forum/windows_10-power-winpc/laptop-wakes-up-with-lid-closed/0b8fd8c2-6afd-442e-ac19-39461b09b0d9?auth=1



https://stackoverflow.com/questions/2916758/running-a-batch-file-with-parameters-in-python-or-f


